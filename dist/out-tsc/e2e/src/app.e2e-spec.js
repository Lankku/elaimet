"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_po_1 = require("./app.po");
describe('workspace-project App', function () {
    var page;
    beforeEach(function () {
        page = new app_po_1.AppPage();
    });
    it('should display welcome message', function () {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('Welcome to elain!');
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmUyZS1zcGVjLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vZTJlL3NyYy9hcHAuZTJlLXNwZWMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxtQ0FBbUM7QUFFbkMsUUFBUSxDQUFDLHVCQUF1QixFQUFFO0lBQ2hDLElBQUksSUFBYSxDQUFDO0lBRWxCLFVBQVUsQ0FBQztRQUNULElBQUksR0FBRyxJQUFJLGdCQUFPLEVBQUUsQ0FBQztJQUN2QixDQUFDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyxnQ0FBZ0MsRUFBRTtRQUNuQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDbEIsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDL0QsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFwcFBhZ2UgfSBmcm9tICcuL2FwcC5wbyc7XG5cbmRlc2NyaWJlKCd3b3Jrc3BhY2UtcHJvamVjdCBBcHAnLCAoKSA9PiB7XG4gIGxldCBwYWdlOiBBcHBQYWdlO1xuXG4gIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgIHBhZ2UgPSBuZXcgQXBwUGFnZSgpO1xuICB9KTtcblxuICBpdCgnc2hvdWxkIGRpc3BsYXkgd2VsY29tZSBtZXNzYWdlJywgKCkgPT4ge1xuICAgIHBhZ2UubmF2aWdhdGVUbygpO1xuICAgIGV4cGVjdChwYWdlLmdldFBhcmFncmFwaFRleHQoKSkudG9FcXVhbCgnV2VsY29tZSB0byBlbGFpbiEnKTtcbiAgfSk7XG59KTtcbiJdfQ==