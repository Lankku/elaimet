"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Animal = /** @class */ (function () {
    function Animal(id, name, sound) {
        this.id = id;
        this.name = name;
        this.sound = sound;
    }
    Animal.prototype.setSound = function (value) {
        this.sound = value;
        return this;
    };
    return Animal;
}());
exports.Animal = Animal;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5pbWFsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vc3JjL2FwcC9tb2RlbHMvYW5pbWFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7SUFDRSxnQkFDUyxFQUFVLEVBQ1YsSUFBWSxFQUNaLEtBQWM7UUFGZCxPQUFFLEdBQUYsRUFBRSxDQUFRO1FBQ1YsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUNaLFVBQUssR0FBTCxLQUFLLENBQVM7SUFDckIsQ0FBQztJQUVNLHlCQUFRLEdBQWYsVUFBZ0IsS0FBYztRQUM1QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUNILGFBQUM7QUFBRCxDQUFDLEFBWEgsSUFXRztBQVhVLHdCQUFNIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEFuaW1hbCB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBpZDogbnVtYmVyLFxuICAgIHB1YmxpYyBuYW1lOiBzdHJpbmcsXG4gICAgcHVibGljIHNvdW5kOiBib29sZWFuXG4gICl7fVxuXG4gICAgcHVibGljIHNldFNvdW5kKHZhbHVlOiBib29sZWFuKTogQW5pbWFsIHtcbiAgICAgIHRoaXMuc291bmQgPSB2YWx1ZTtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbiAgfSJdfQ==