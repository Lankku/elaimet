"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animal_1 = require("../models/animal");
var AnimalsComponent = /** @class */ (function () {
    function AnimalsComponent() {
        this.animals = [
            new animal_1.Animal(1, 'horse', false),
            new animal_1.Animal(2, 'sheep', false),
            new animal_1.Animal(3, 'cow', false),
            new animal_1.Animal(4, 'pig', false)
        ];
    }
    AnimalsComponent.prototype.onCardClick = function (animal) {
        if (animal.sound) {
            this.playAudio(animal.name);
            animal.setSound(false);
        }
        else {
            this.playAudio(animal.id);
            animal.setSound(true);
        }
    };
    AnimalsComponent.prototype.playAudio = function (file) {
        var audio = new Audio();
        audio.src = "assets/audio/" + file + ".wav";
        audio.load();
        audio.play();
    };
    AnimalsComponent.prototype.ngOnInit = function () {
    };
    AnimalsComponent = __decorate([
        core_1.Component({
            selector: 'app-animals',
            templateUrl: './animals.component.html',
            styleUrls: ['./animals.component.css']
        }),
        __metadata("design:paramtypes", [])
    ], AnimalsComponent);
    return AnimalsComponent;
}());
exports.AnimalsComponent = AnimalsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5pbWFscy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvYXBwL2FuaW1hbHMvYW5pbWFscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBa0Q7QUFDbEQsMkNBQTBDO0FBTzFDO0lBMkJFO1FBekJBLFlBQU8sR0FBYTtZQUNsQixJQUFJLGVBQU0sQ0FBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLEtBQUssQ0FBQztZQUMzQixJQUFJLGVBQU0sQ0FBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLEtBQUssQ0FBQztZQUMzQixJQUFJLGVBQU0sQ0FBQyxDQUFDLEVBQUMsS0FBSyxFQUFDLEtBQUssQ0FBQztZQUN6QixJQUFJLGVBQU0sQ0FBQyxDQUFDLEVBQUMsS0FBSyxFQUFDLEtBQUssQ0FBQztTQUMxQixDQUFDO0lBb0JjLENBQUM7SUFqQmpCLHNDQUFXLEdBQVgsVUFBWSxNQUFNO1FBQ2hCLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVCLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDMUIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QixDQUFDO0lBQ0gsQ0FBQztJQUVELG9DQUFTLEdBQVQsVUFBVSxJQUFJO1FBQ1osSUFBSSxLQUFLLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUN4QixLQUFLLENBQUMsR0FBRyxHQUFHLGVBQWUsR0FBRyxJQUFJLEdBQUcsTUFBTSxDQUFDO1FBQzVDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNiLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFJRCxtQ0FBUSxHQUFSO0lBRUEsQ0FBQztJQS9CVSxnQkFBZ0I7UUFMNUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFdBQVcsRUFBRSwwQkFBMEI7WUFDdkMsU0FBUyxFQUFFLENBQUMseUJBQXlCLENBQUM7U0FDdkMsQ0FBQzs7T0FDVyxnQkFBZ0IsQ0FpQzVCO0lBQUQsdUJBQUM7Q0FBQSxBQWpDRCxJQWlDQztBQWpDWSw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQW5pbWFsIH0gZnJvbSAnLi4vbW9kZWxzL2FuaW1hbCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1hbmltYWxzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2FuaW1hbHMuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9hbmltYWxzLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBBbmltYWxzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBhbmltYWxzOiBBbmltYWxbXSA9IFtcbiAgICBuZXcgQW5pbWFsKDEsJ2hvcnNlJyxmYWxzZSksXG4gICAgbmV3IEFuaW1hbCgyLCdzaGVlcCcsZmFsc2UpLFxuICAgIG5ldyBBbmltYWwoMywnY293JyxmYWxzZSksXG4gICAgbmV3IEFuaW1hbCg0LCdwaWcnLGZhbHNlKVxuICBdO1xuXG5cbiAgb25DYXJkQ2xpY2soYW5pbWFsKSB7XG4gICAgaWYgKGFuaW1hbC5zb3VuZCkge1xuICAgICAgdGhpcy5wbGF5QXVkaW8oYW5pbWFsLm5hbWUpO1xuICAgICAgYW5pbWFsLnNldFNvdW5kKGZhbHNlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5wbGF5QXVkaW8oYW5pbWFsLmlkKTtcbiAgICAgIGFuaW1hbC5zZXRTb3VuZCh0cnVlKTsgXG4gICAgfVxuICB9XG5cbiAgcGxheUF1ZGlvKGZpbGUpIHtcbiAgICBsZXQgYXVkaW8gPSBuZXcgQXVkaW8oKTtcbiAgICBhdWRpby5zcmMgPSBcImFzc2V0cy9hdWRpby9cIiArIGZpbGUgKyBcIi53YXZcIjtcbiAgICBhdWRpby5sb2FkKCk7XG4gICAgYXVkaW8ucGxheSgpO1xuICB9XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBcbiAgfVxuXG59XG4iXX0=