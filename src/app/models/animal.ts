export class Animal {
  constructor(
    public id: number,
    public name: string,
    public sound: boolean
  ){}

    public setSound(value: boolean): Animal {
      this.sound = value;
      return this;
    }
  }