import { Component, OnInit } from '@angular/core';
import { Animal } from '../models/animal';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.css']
})
export class AnimalsComponent implements OnInit {

  animals: Animal[] = [
    new Animal(1,'horse',false),
    new Animal(2,'sheep',false),
    new Animal(3,'cow',false),
    new Animal(4,'pig',false)
  ];


  onCardClick(animal) {
    if (animal.sound) {
      this.playAudio(animal.name);
      animal.setSound(false);
    } else {
      this.playAudio(animal.id);
      animal.setSound(true); 
    }
  }

  playAudio(file) {
    let audio = new Audio();
    audio.src = "assets/audio/" + file + ".wav";
    audio.load();
    audio.play();
  }

  constructor() { }

  ngOnInit() {
    
  }

}
